<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Welcome to login</h1>
	<br>
	
	<form action="${pageContext.request.contextPath}/api/user" method="post">
		<label>User Name : </label><input type="text" name="userName" placeholder="Enter user name..."><br>
		<label>Password : </label><input type="password" name="password" placeholder="Enter password..."><br>
		<input type="submit" value="Login">
	</form>
	
	<br>
	<a href="${pageContext.request.contextPath}/">Index Page</a>
</body>
</html>