package com.java.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.model.User;
import com.java.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
	public void insertUser(User user) {
		repository.save(user);
	}
	
	public User getUserById(Long id) {
		return repository.findById(id).get();
	}
	
	public User getUserByName(String username) {
		return repository.findAll().stream()
			.filter(user -> user.getUserName().equals(username))
			.findFirst().get();
	}
	
	public boolean isUserValidate(User user) {
		return getUserByName(user.getUserName()).getPassword().equals(user.getPassword());
	}
	
}
