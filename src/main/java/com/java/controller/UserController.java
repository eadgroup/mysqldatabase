package com.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.java.model.User;
import com.java.service.UserService;

@Controller
@RequestMapping("api/user")
public class UserController {

	@Autowired
	private UserService service;

//	@PostMapping
//	public void saveUser(@RequestBody User user) {
//		service.insertUser(user);
//	}

	@GetMapping(path = "{id}")
	public User getUserById(@RequestParam Long id) {
		return service.getUserById(id);
	}

	@PostMapping
	public String isAValidUser(User user) {
		if (service.isUserValidate(user)) {
			return "index";
		}
		return "login";
	}
}
